import React from 'react';


class Toggle extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          isToggleOn: true,
          numberClick: 0
    };
  
      // Cette liaison est nécéssaire afin de permettre
      // l'utilisation de `this` dans la fonction de rappel.
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(state => ({
        numberClick: this.state.numberClick + 1,
        isToggleOn: !state.isToggleOn
      }));
    }
  
    render() {
      return (
         <div>
            <button onClick={this.handleClick}>
            {this.state.isToggleOn ? 'ON' : 'OFF'}
            </button>
            <br/>
        Nombre de clics : {this.state.numberClick}
        </div>

      );
    }
  }

export default Toggle;