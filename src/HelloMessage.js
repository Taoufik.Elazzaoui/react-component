import React from 'react';


class HelloMessage extends React.Component {

    render() {
      const {name, surname, color} = this.props
      const divStyle = {
        color: color,
      };

      return (
        <div style={divStyle}>
          Salut {name} {surname} {surname ? surname.length : 'surname undefined'}
        </div>
      );
    }
  }

  export default HelloMessage;