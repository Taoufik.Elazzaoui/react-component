import React from 'react';


class Timer extends React.Component {
    constructor(props) {
      super(props);
      this.state = { seconds: 0 };
    }
  
    tick() {
      this.setState(state => ({
        seconds: state.seconds + 1 
      }));
    }
  
    componentDidMount() {
      const delay = this.props.delay
      const realDelay = delay ? delay : 1000
      this.interval = setInterval(() => this.tick(), realDelay);
    }
  
    componentWillUnmount() {
      clearInterval(this.interval);
    }
  
    render() {
      return (
        <div>
          Secondes : {this.state.seconds}
        </div>
      );
    }
  }
  
export default Timer;