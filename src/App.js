import logo from './logo.svg';
import './App.css';
import HelloMessage from './HelloMessage.js'
import Timer from './Timer.js'
import Toggle from './Toggle.js'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <br/>
        <HelloMessage name='Taoufik' surname='El Azzaoui'/>
        <HelloMessage name='Taoufik' color='cyan'/>

      <Timer delay='5000' />
      <Timer />
      <br/>
      <Toggle/>
      </header>
    </div>
  );
}

export default App;
